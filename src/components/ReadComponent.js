import {
  Table,
  Tbody,
  Thead,
  Th,
  Tr,
  Td,
  Box,
  Heading,
  Button,
  Flex,
  TableCaption,
} from "@chakra-ui/react";
import axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";

const ReadComponent = () => {
  const navigate = useNavigate();
  const [info, setInfo] = useState([]);

  const readData = async () => {
    const { data } = await axios.get(
      "https://64013d2a0a2a1afebee7b695.mockapi.io/crud_operation"
    );
    setInfo(data);
    readData();
  };

  useEffect(() => {
    readData();
  }, [readData]);

  const handleBack = () => {
    navigate("/");
  };

  const handleDelete = async (id) => {
    const { data } = await axios.delete(
      `https://64013d2a0a2a1afebee7b695.mockapi.io/crud_operation/${id}`
    );
    readData();
  };

  const handleEdit = (id, name, email) => {
    localStorage.setItem("id", id);
    localStorage.setItem("name", name);
    localStorage.setItem("email", email);
    navigate(`/edit/${id}`);
  };
  return (
    <Box w="70%" textAlign="center" border="1px solid black" mt="10%">
      <Heading as="h1" fontSize="2xl" bgColor="#800000" color="white" p="4">
        Reading Data from the backend
        <Button float="right" variant="outline" onClick={handleBack}>
          Back
        </Button>
      </Heading>
      <Table>
        <TableCaption>Practice CRUD operation by your own.</TableCaption>
        <Thead>
          <Tr>
            <Th>ID</Th>
            <Th>Name</Th>
            <Th>Email</Th>
            <Th>Actions</Th>
          </Tr>
        </Thead>
        {info.map((element, i) => (
          <Tbody>
            <Tr key={i}>
              <Td>{element.id}</Td>
              <Td>{element.name}</Td>
              <Td>{element.email}</Td>
              <Td>
                <Flex justifyContent="center" alignItems="center" gap="5">
                  <Button
                    colorScheme="red"
                    onClick={() => handleDelete(element.id)}
                  >
                    Delete
                  </Button>
                  <Button
                    colorScheme="teal"
                    onClick={() =>
                      handleEdit(element.id, element.name, element.email)
                    }
                  >
                    Edit
                  </Button>
                </Flex>
              </Td>
            </Tr>
          </Tbody>
        ))}
      </Table>
    </Box>
  );
};
export default ReadComponent;
