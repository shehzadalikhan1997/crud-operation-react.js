import {
  Box,
  Flex,
  FormControl,
  FormLabel,
  Input,
  Button,
  Heading,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { Link as RouterLink, useNavigate } from "react-router-dom";
import axios from "axios";

const HomeComponent = () => {
  const navigate = useNavigate();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");

  const header = { "Access-Control-Allow-Origin": "*" };

  const submitHandler = async (e) => {
    e.preventDefault();
    if (name.length !== 0 && email.length !== 0) {
      const { data } = await axios.post(
        "https://64013d2a0a2a1afebee7b695.mockapi.io/crud_operation",
        { name, email, header }
      );

      setName("");
      setEmail("");
      navigate("/read");
    }
  };
  const handleDataShow = () => {
    navigate("/read");
  };

  return (
    <Box m="auto" w="60%" border="1px solid black" p="5" background="#fcd6d5">
      <Heading
        as="h1"
        fontSize="3xl"
        background="#800000"
        py="3"
        rounded="lg"
        color="white"
        textAlign="center"
        pl="20"
      >
        CRUD Opeartion
        <Button
          display="inline"
          float="right"
          mr="5"
          variant="outline"
          onClick={handleDataShow}
        >
          Show Data
        </Button>
      </Heading>

      <Flex py="5">
        <FormControl
          id="tasks"
          display="flex"
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
        >
          <Box
            display="flex"
            gap={5}
            w="70%"
            justifyContent="center"
            alignItems="center"
          >
            <FormLabel
              htmlFor="tasks"
              as="h1"
              fontSize="lg"
              fontWeight="bold"
              w="20"
            >
              Name :
            </FormLabel>
            <Input
              border="1px solid black"
              background="white"
              type="text"
              placeholder="Enter your name here..."
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </Box>
          <Box
            display="flex"
            gap={5}
            w="70%"
            justifyContent="center"
            alignItems="center"
            pt="5"
          >
            <FormLabel
              htmlFor="email"
              as="h1"
              fontSize="lg"
              fontWeight="bold"
              w="20"
            >
              Email :
            </FormLabel>
            <Input
              border="1px solid black"
              background="white"
              type="text"
              placeholder="Enter your name here..."
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Box>
          <Button
            mt="4"
            bgColor="#800000"
            color="white"
            type="submit"
            onClick={submitHandler}
          >
            Submit
          </Button>
        </FormControl>
      </Flex>
    </Box>
  );
};

export default HomeComponent;
