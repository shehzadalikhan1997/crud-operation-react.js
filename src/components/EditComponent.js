import {
  Box,
  Flex,
  FormControl,
  FormLabel,
  Input,
  Button,
  Heading,
  TableCaption,
} from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import axios from "axios";
import { useParams } from "react-router-dom";

const EditComponent = () => {
  const navigate = useNavigate();
  const { id } = useParams();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");

  useEffect(() => {
    setName(localStorage.getItem("name"));
    setEmail(localStorage.getItem("email"));
  }, []);

  const updateHandler = async (e) => {
    e.preventDefault();
    const { data } = await axios.put(
      `https://64013d2a0a2a1afebee7b695.mockapi.io/crud_operation/${id}`,
      { name, email }
    );
    navigate("/read");
    return data;
  };
  const handleBack = () => {
    navigate("/");
  };
  return (
    <Box m="auto" w="60%" border="1px solid black" p="5">
      <Heading
        as="h1"
        fontSize="3xl"
        display="flex"
        justifyContent="center"
        background="#800000"
        py="3"
        rounded="lg"
        color="white"
      >
        Update Data
        <Button float="right" variant="outline" onClick={handleBack}>
          Back
        </Button>
      </Heading>
      <Flex py="5">
        <FormControl
          id="tasks"
          display="flex"
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
        >
          <Box
            display="flex"
            gap={5}
            w="70%"
            justifyContent="center"
            alignItems="center"
          >
            <FormLabel
              htmlFor="tasks"
              as="h1"
              fontSize="lg"
              fontWeight="bold"
              w="20"
            >
              Name :
            </FormLabel>
            <Input
              id="inp"
              type="text"
              placeholder="Enter your name here..."
              value={name}
              onChange={(e) => setName(e.target.value)}
            />
          </Box>
          <Box
            display="flex"
            gap={5}
            w="70%"
            justifyContent="center"
            alignItems="center"
            pt="5"
          >
            <FormLabel
              htmlFor="email"
              as="h1"
              fontSize="lg"
              fontWeight="bold"
              w="20"
            >
              Email :
            </FormLabel>
            <Input
              id="inp"
              type="text"
              placeholder="Enter your name here..."
              value={email}
              onChange={(e) => setEmail(e.target.value)}
            />
          </Box>
          <Button
            mt="4"
            bgColor="#800000"
            color="white"
            type="submit"
            onClick={updateHandler}
          >
            Update
          </Button>
        </FormControl>
      </Flex>
    </Box>
  );
};

export default EditComponent;
