import { Flex, Box } from "@chakra-ui/react";
import HomeComponent from "./components/HomeComponent";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import ReadComponent from "./components/ReadComponent";
import EditComponent from "./components/EditComponent";

function App() {
  return (
    <>
      <Box shadow="10">
        <Flex justifyContent="center" alignItems="center">
          <BrowserRouter>
            <Routes>
              <Route path="/" element={<HomeComponent />} />
              <Route path="/read" element={<ReadComponent />} />
              <Route path="/edit/:id" element={<EditComponent />} />
            </Routes>
          </BrowserRouter>
        </Flex>
      </Box>
    </>
  );
}

export default App;
